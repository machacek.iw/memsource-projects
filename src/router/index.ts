import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import { Projects, ProjectCreate, ProjectEdit } from '../pages/Projects';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'projects',
    component: Projects,
  },
  {
    path: '/success',
    name: 'projects-success',
    component: Projects,
  },
  {
    path: '/projects/new',
    name: 'projects-new',
    component: ProjectCreate,
  },
  {
    path: '/projects/:id',
    name: 'projects-edit',
    component: ProjectEdit,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
