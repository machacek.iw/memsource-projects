export interface Filter {
  name: string;
  label: string;
  operator: string;
  value: string;
}

export interface SortItem {
  id: string | null;
  field: string | null;
  label: string | null;
  asc: boolean;
}

export interface Project {
  id: number;
  name: string;
  targetLanguages: Array<string>;
}

export interface ProjectForm {
  name: string;
  sourceLanguage: string;
  targetLanguages: Array<string>;
  status: string;
  dateCreated: string;
  dateUpdated: string;
  dateDue: string;
}

export interface InputResults {
  key: string;
  value: string;
}

export interface Alert {
  content: string;
  type: string;
  isVisible: boolean;
}
