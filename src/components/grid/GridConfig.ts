import { Filter, SortItem } from '@/types';

interface Optional {
  title?: string | null;
  filters?: Array<Filter> | [];
  sortList?: Array<SortItem> | [];
}

export interface Column {
  key: string;
  label: string;
  formatType?: string;
}

export interface GridRow {
  [key: string]: string;
}

export default class GridConfig {
  public name: string

  public title: string | null

  public filters: Array<Filter>

  public columns: Array<Column>

  public sortList: Array<SortItem>

  constructor(
    name: string,
    columns: Array<Column>,
    {
      title = null,
      filters = [],
      sortList = [],
    }: Optional = {} as Optional,
  ) {
    this.name = name;
    this.title = title;
    this.filters = filters;
    this.columns = columns;
    this.sortList = sortList;
  }
}
