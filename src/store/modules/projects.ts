import {
  VuexModule, Module, Mutation, Action,
} from 'vuex-module-decorators';
import { GridRow } from '@/components/grid/GridConfig';
import axios from 'axios';
import _ from 'lodash';

@Module({ namespaced: true })
class Projects extends VuexModule {
  public projects: Array<GridRow> = []

  @Mutation
  public setProjects(projects: Array<GridRow>): void {
    this.projects = projects;
  }

  @Action
  public async ensureProjects(): Promise<void> {
    const { data } = await axios.get('/projects');
    const projects = _.get(data, '_embedded.projects');
    this.context.commit('setProjects', projects);
  }
}
export default Projects;
