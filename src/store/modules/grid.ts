import {
  VuexModule, Module, Mutation, Action,
} from 'vuex-module-decorators';
import { GridRow } from '@/components/grid/GridConfig';
import { Filter as FilterTypes, SortItem } from '@/types';
import _ from 'lodash';

@Module({ namespaced: true })
class Grid extends VuexModule {
  public data: Array<GridRow> = []

  public activeGridData: Array<GridRow> = []

  public filters: Array<FilterTypes> = []

  public sortBy: SortItem | null = null

  get activeData() {
    return this.activeGridData;
  }

  @Mutation
  public setData(data: Array<GridRow>): void {
    this.data = data;
  }

  @Mutation
  public setActiveData(data: Array<GridRow>): void {
    this.activeGridData = data;
  }

  @Mutation
  public setFilters(filters: Array<FilterTypes>): void {
    this.filters = filters;
  }

  @Mutation
  public setSort(sortItem: SortItem): void {
    this.sortBy = sortItem;
  }

  @Action
  public setGridData(data: Array<GridRow>): void {
    this.context.commit('setData', data);
    this.context.dispatch('refreshActiveData');
  }

  @Action
  public refreshActiveData() {
    let activeData = [...this.data];

    // Filter
    _.forEach(this.filters, (filter) => {
      const searchValue = _.get(filter, 'value', null);
      const field = _.get(filter, 'name', null);

      if (!searchValue) return;

      activeData = activeData.filter(
        (data) => {
          if (_.has(data, field) && !_.isNull(data[field])) {
            return data[field].toLowerCase().includes(searchValue.toLowerCase());
          }
          return false;
        },
      );
    });

    // Sort
    if (this.sortBy) {
      const sortDirection = this.sortBy.asc ? 'asc' : 'desc';
      activeData = _.orderBy(activeData, [this.sortBy.field], [sortDirection]);
    }

    this.context.commit('setActiveData', activeData);
  }

  @Action
  public setGridFilters(filters: Array<FilterTypes>): void {
    this.context.commit('setFilters', filters);
    this.context.dispatch('refreshActiveData');
  }

  @Action
  public setGridSort(sortItem: SortItem): void {
    this.context.commit('setSort', sortItem);
    this.context.dispatch('refreshActiveData');
  }
}
export default Grid;
