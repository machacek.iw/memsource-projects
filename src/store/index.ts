import Vue from 'vue';
import Vuex from 'vuex';
import projects from '@/store/modules/projects';
import grid from '@/store/modules/grid';

Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    projects,
    grid,
  },
});
export default store;
