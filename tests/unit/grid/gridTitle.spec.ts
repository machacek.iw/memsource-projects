import { shallowMount } from '@vue/test-utils';
import GridTitle from '@/components/grid/gridHeader/GridTitle.vue';

describe('GridTitle.vue', () => {
  it('renders title', () => {
    const wrapper = shallowMount(GridTitle, {
      propsData: {
        title: 'Title',
      },
    });

    expect(wrapper.find('.grid__title').text()).toEqual('Title');
  });
});
